<?php

class Controller_Grade1 extends Controller {

    public function __construct()
    {
        $this->model = new Model_Grade1();
        $this->view = new View();
    }

    public function action_index()
    {
        $this->view->setPageTitle('Grade1 - Test page');
        $data = $this->model->getData();
        $this->view->generate('grade1_view.php', 'template_view.php', $data);
    }

}
