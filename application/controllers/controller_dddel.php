<?php

class Controller_Dddel extends Controller {

    public function __construct()
    {
        $this->model = new Model_Dddel();
        $this->view = new View();
    }

    public function action_index()
    {
        $this->view->setPageTitle('DEL Entities');
        $data = $this->model->getData();
        $this->view->generate('dddel_view.php', 'template_view.php', $data);
    }

}
