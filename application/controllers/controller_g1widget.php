<?php

class Controller_G1widget extends Controller {

    public function __construct()
    {
        $this->model = new Model_G1widget();
        $this->view = new View();
    }

    public function action_index()
    {
        if (empty($_POST['test']) && empty($_POST['ids'])) {
            $this->view->setPageTitle('Grade1 - Widget page');
            $data = $this->model->getData();
            $this->view->generate('g1widget_view.php', 'template_view.php', $data);
        } else {
            header('Access-Control-Allow-Origin: *');

            if (!empty($_POST['ids'])) {
                $arrayByString = explode(',', $_POST['ids']);
                Auth::authorisation();
                $leadsArray = $this->getEntityById(API_LEADS, $arrayByString);
                $leadsArray = [$this->getListLeads($leadsArray)];
                $column = [];

                foreach ($leadsArray[0][0] as $key => $val) {
                    $column[] = $key;
                }

                array_shift($column);
                $columnString = implode(";", $column)."\n";
                $arrayData = [];

                foreach ($leadsArray[0] as $key => $val) {
                    $arrayData[] = $val['name'].';'.
                        $val['create'].';'.
                        $val['company'].';'.
                        $val['tags'].';'.
                        $val['custom'].';'.
                        $val['contacts'];
                }

                $columnStringSecond = implode("\n", $arrayData);
                $name = 'leads.csv';
                $fp = fopen( $name, 'w');

                if ($fp) {
                    $str = $columnString . $columnStringSecond;
                    fwrite($fp, $str);
                    echo 'File '.$name.' записан!';
                } else {
                    echo 'File '.$name.' не записан!';
                }

                fclose($fp);
            }
        }
    }

    /**
     * @param $api
     * @param $arrId
     * @return array $result
     */
    public function getEntityById($api, $arrId)
    {
        $objEntity = new Widget();

        if (is_array($arrId) && sizeof($arrId) > 0) {
            $result = [];

            foreach ($arrId as $id) {
                $result[] = $objEntity->getEntitiesById($api, $id);
            }
        }

        return $result;
    }

    /**
     * @param $leadsArray
     * @return array $result
     */
    public function getEntityIdByArray($leadsArray)
    {
        foreach ($leadsArray as $lead) {
            $lead = $lead[0];
            $result['companies'][] = $lead['company']['id'];

            foreach ($lead['contacts']['id'] as $conts) {
                $result['contacts'][] = $conts;
            }
        }

        $result['companies_uid'] = implode(',', array_unique($result['companies']));
        $result['contacts_uid'] = implode(',', array_unique($result['contacts']));
        $result['companies'] = implode(',', $result['companies']);
        $result['contacts'] = implode(',', $result['contacts']);

        return $result;
    }

    /**
     * @param $leadsArray
     * @return array $result
     */
    public function getListLeads($leadsArray)
    {
        $arr = $this->getEntityIdByArray($leadsArray);
        $tempArray['comps'] = $this->getEntityName(API_COMPANIES, $arr['companies_uid']);
        $tempArray['conts'] = $this->getEntityName(API_CONTACTS, $arr['contacts_uid']);

        if (is_array($leadsArray) && sizeof($leadsArray) > 0) {
            $result = [];

            foreach ($leadsArray as $lead) {
                $lead = $lead[0];
                $tags = $this->getValueFromArrayToString($lead['tags'], 'name');
                $custom = $this->getValueFromArrayToString($lead['custom_fields'], ['name', 'values', 'value']);
                $cont = $this->getValueFromArrayToString($lead['contacts']['id'], '', $leadsArray);
                $result[] = [
                    'id' => '"'.$lead['id'].'"',
                    'name' => '"'.$lead['name'].'"',
                    'create' => '"'.date("d-m-Y H:i:s", $lead['created_at']).'"',
                    'company' => '"'.($this->getNameByIdInString($lead['company']['id'], $tempArray['comps'])).'"',
                    'tags' => $tags,
                    'custom' => $custom,
                    'contacts' => $cont
                ];
            }
        }

        return $result;
    }

    /**
     * @param $API
     * @param $arrayData
     * @return array $result
     */
    public function getEntityName($API, $arrayData)
    {
        $data = $this->getEntityById($API, [$arrayData]);
        $names = [];

        foreach ($data as $key) {
            foreach ($key as $entity) {
                $names[$entity['id']] = $entity['name'];
            }
        }

        return $names;
    }

    /**
     * @param $tag_arr
     * @param $keyValue
     * @param null $leadsArray
     * @return string $tags
     */
    public function getValueFromArrayToString($tag_arr, $keyValue, $leadsArray = null)
    {
        if (is_array($leadsArray)) {
            $arr = $this->getEntityIdByArray($leadsArray);
            $tempArray['conts'] = $this->getEntityName(API_CONTACTS, $arr['contacts_uid']);
        }

        $i = 0;
        $tags = '';

        foreach ($tag_arr as $tag) {
            if (is_array($keyValue) && (sizeof($keyValue) > 1)) {
                $tags .= '"'.$tag[$keyValue[0]].':';
                $j = 0;

                foreach ($tag[$keyValue[1]] as $key => $field) {
                    $tags .= $field['value'];
                    $j++;
                    if ($j < sizeof($tag[$keyValue[1]])) {
                        $tags .= ', ';
                    }
                }

                $tags .= '"';
            } else {
                if (!empty($keyValue)) {
                    $tags .= '"'.$tag[$keyValue].'"';
                } else {
                    $tags .= '"'.($this->getNameByIdInString($tag, $tempArray['conts'])).'"';
                }
            }

            if ($i < sizeof($tag_arr) - 1) {
                $tags .= ', ';
            }

            $i++;
        }

        return $tags;
    }

    /**
     * @param $arrId
     * @param $namesArray
     * @return array $result
     */
    public function getNameByIdInArray($arrId, $namesArray)
    {
        $arr = explode(',', $arrId);

        foreach ($arr as $key => $val) {
            $result[] = $namesArray[$val];
        }

        return $result;
    }

    /**
     * @param $id
     * @param $namesArray
     * @return string $result
     */
    public function getNameByIdInString($id, $namesArray)
    {
        foreach ($namesArray as $key => $val) {
            $result = $namesArray[$id];
        }

        return $result;
    }

}
