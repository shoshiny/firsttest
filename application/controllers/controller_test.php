<?php

class Controller_Test extends Controller {

    public function __construct()
    {
        $this->model = new Model_Test();
        $this->view = new View();
    }
	
    public function action_index()
    {
        $this->view->setPageTitle('Test Page');
		$data = $this->model->getData();
        $this->view->generate('test_view.php', 'template_view.php', $data);
    }

}
