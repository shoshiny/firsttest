<?php

class Controller_Export extends Controller {

    public function __construct()
    {
        $this->model = new Model_Export();
        $this->view = new View();
    }

    public function action_index()
    {
        $this->view->setPageTitle('Export Page');
		$data = $this->model->getData();
        $this->view->generate('export_view.php', 'template_view.php', $data);
    }

}
