<?php

class Model_Grade1 extends Model {

    /** create content and get result
     * @return array
     */
    public function getData()
    {
        $result[] = '<small>[ ' . date('H:i:s') . ' --> ' . microtime() . ' ]</small> <b>begin after submit</b><br>';
        $auth = Auth::authorisation();

        if (empty($auth['err'])) {
            $result[] = $auth;

            // create only base entity
            if ($this->validateFields($_POST, 'number,sendonly')) {
                $result[] = 'begin before create_entity<br>';

                foreach ($this->addEntity() as $key => $val) {
                    $result[] = $val;
                }
            } elseif ($this->validateFields($_POST, 'number,sendonly', true)) {
                $result[] = 'Data not correct! Reinput!1';
            }

            if ($this->validateFields($_POST, 'number,send')) {
                $result[] = 'begin before create_entity<br>';
                
                foreach ($this->addEntity() as $key => $val) {
                    $result[] = $val;
                }
            } elseif ($this->validateFields($_POST, 'number,send', true)) {
                $result[] = 'Data not correct! Reinput!2';
            }

            if ($this->validateFields($_POST, 'addText,addtext,identity,entitytype')) {
                foreach ($this->addText() as $key => $val) {
                    $result[] = $val;
                }
            } elseif ($this->validateFields($_POST, 'addText,addtext,identity,entitytype', true)) {
                $result[] = 'Data not correct! Reinput!3';
            }

            if ($this->validateFields($_POST, 'addNote, addtext, identity')) {
                if ((int) $_POST['rbut'] === 1) {
                    foreach ($this->addNote() as $key => $val) {
                        $result[] = $val;
                    }
                } elseif ((int) $_POST['rbut'] === 2 || (int) $_POST['rbut'] === 24) {
                    foreach ($this->addPhone() as $key => $val) {
                        $result[] = $val;
                    }
                }
            } elseif ($this->validateFields($_POST, 'addNote, addtext, identity', true)) {
                $result[] = 'Data not correct! Reinput!4';
            }

            if ($this->validateFields($_POST, 'addNote100, addText, identity')) {
                $k = 100;

                if ((int) $_POST['rbut'] === 1) {
                    while($k > 0) {
                        foreach ($this->addNote() as $key => $val) {
                            $result[] = $val;
                        }
                        $k--;
                    }
                } elseif ((int) $_POST['rbut'] === 2) {
                    while($k > 0) {
                        foreach ($this->addPhone() as $key => $val) {
                            $result[] = $val;
                        }
                        $k--;
                    }
                }
            } elseif ($this->validateFields($_POST, 'addNote100, addText, identity', true)) {
                $result[] = 'Data not correct! Reinput!5';
            }

            if ($this->validateFields($_POST, 'addNote200, addText, identity')) {
                $k = 200;
                if ((int) $_POST['rbut'] === 1) {
                    while($k > 0) {
                        foreach ($this->addNote() as $key => $val) {
                            $result[] = $val;
                        }
                        $k--;
                    }
                } elseif ((int) $_POST['rbut'] === 2) {
                    while($k > 0) {
                        foreach ($this->addPhone() as $key => $val) {
                            $result[] = $val;
                        }
                        $k--;
                    }
                } elseif ((int) $_POST['rbut'] === 24) {
                    while($k > 0) {
                        foreach ($this->addPhone() as $key => $val) {
                            $result[] = $val;
                        }
                        $k--;
                    }
                }
            } elseif ($this->validateFields($_POST, 'addNote200, addText, identity', true)) {
                $result[] = 'Data not correct! Reinput!6';
            }

            if ($this->validateFields($_POST, 'addTask, identityTask, addtime, addtexttask, iduser')) {
                $result[] = $this->addTask();
            } elseif ($this->validateFields($_POST, 'addTask, identityTask, addtime, addtexttask, iduser', true)) {
                $result[] = 'Data not correct! Reinput!7';
            }
    
            if ($this->validateFields($_POST, 'add500, addtime, addtexttask, iduser')) {
                $k = 500;
                $temp = [];

                while($k > 0) {
                    if ($k % 100 === 0) {
                        sleep(1);
                    }
                    $temp[] = $this->addTask();
                    $k--;
                }

                $result[] = $temp;
            } elseif ($this->validateFields($_POST, 'add500, addtime, addtexttask, iduser', true)) {
                $result[] = 'Data not correct! Reinput!8';
            }

            if ($this->validateFields($_POST, 'add100, addtime, addtexttask, iduser')) {
                $k = 100;

                while($k > 0) {
                    if ($k % 50 === 0) {
                        sleep(1);
                    }
                    $result[] = $this->addTask();
                    $k--;
                }
            } elseif ($this->validateFields($_POST, 'add100, addtime, addtexttask, iduser', true)) {
                $result[] = 'Data not correct! Reinput!9';
            }

            if ($this->validateFields($_POST, 'endTask, idtask')) {
                $result[] = $this->endTask();
            } elseif ($this->validateFields($_POST, 'endTask, idtask', true)) {
                $result[] = 'Data not correct! Reinput!10';
            }

            $result[] = '<small>[ ' . date('H:i:s') . ' --> ' . microtime() . ' ]</small> <b>end</b><br>';

            if (!isset($result)) {
                $result = [];
            }

        } else {
            $result = array_merge($result, $auth);
        }

        return $result;
    }

    /**add entity into API
     * @return array
     */
    public function addEntity()
    {
        $numberPost = $_POST['number'];
        $firstResult = $this->createNewEntity($numberPost);
        $entity = ['company', 'contact', 'customer', 'lead'];

        foreach ($firstResult as $key => $val) {
            $result[] = 'add ---> ' . $entity[$key] . ': ' . sizeof($firstResult[$key][$entity[$key]]['add']);
        }

		if (!isset($_POST['sendonly']) && empty($_POST['sendonly'])) {
			sleep(1);
			$resultAdd = $this->addMultiview(1);
		} else {
			$resultAdd = 0;
        }

        if (is_array($resultAdd)) {
            $result[] = sizeof($resultAdd);
        }

        return $result;
    }

    /**create new entity by numbers post
     * @param $numberPost
     * @return array
     */
    public function createNewEntity($numberPost)
    {
        $arr = [];
        $resultArray = []; // result array
        $objCompany = new Companies($numberPost, 'add');
        $objСontact = new Contacts($numberPost, 'add');
        $objCustomer = new Customers($numberPost, 'add');
        $objLead = new Leads($numberPost, 'add');
        $objCompany->setResultPushEntity($objCompany->getEntity());
        $resultArray = $objCompany->getResultPushEntity(); // get array answer server
        $objCompany->setEntityId($objCompany->setArrayIdNewEntity($resultArray)); // id companes

        $arr[] = ['company' => $objCompany->getEntity()]; // for test info
        unset($resultArray);
//            1/ create entity
//            2/ puah & get ID and add to ecntity
        $resultArray = $objСontact->addIdToEntity($objСontact->getEntity(), $objCompany->getIdEntity()); // add id companes
        $objСontact->setEntity($resultArray);
        $objСontact->setResultPushEntity($objСontact->getEntity());

        $resultArray = $objСontact->getResultPushEntity();//$objСontact->getEntity()

        $objСontact->setEntityId($objСontact->setArrayIdNewEntity($resultArray)); // id cont
//        $objСontact->setEntity($resultArray); //  comment for work right

        $arr[] = ['contact' => $objСontact->getEntity()]; // for test info
        unset($resultArray);

        $resultArray = $objCustomer->addIdToEntity($objCustomer->getEntity(), $objCompany->getIdEntity()); // add id companes
        $objCustomer->setEntity($resultArray);
        $resultArray = $objCustomer->addIdToEntity($objCustomer->getEntity(), $objСontact->getIdEntity(), 'contacts_id'); // add id companes
        $objCustomer->setEntity($resultArray);
        $objCustomer->setResultPushEntity($objCustomer->getEntity());
        $objCustomer->setEntityId($objCustomer->setArrayIdNewEntity($resultArray)); // id cust
        $objCustomer->setEntity($resultArray);

        unset($resultArray);
        $arr[] = ['customer' => $objCustomer->getEntity()];
        $resultArray = $objLead->addIdToEntity($objLead->getEntity(), $objCompany->getIdEntity()); // add id companes
        $objLead->setEntity($resultArray);
        $resultArray = $objLead->addIdToEntity($objLead->getEntity(), $objСontact->getIdEntity(), 'contacts_id'); // add id companes
        $objLead->setEntity($resultArray);
        $objLead->setResultPushEntity($objLead->getEntity());
        $objLead->setEntityId($objLead->setArrayIdNewEntity($resultArray)); // id cust
        $objLead->setEntity($resultArray);
        $arr[] = ['lead' => $objLead->getEntity()];

        return $arr;
    }

    /**
     * @param $numbers
     * @param int $limit
     * @return array $result
     */
    public function addMultiview($numbers, $limit = 0) {
        Auth::authorisation();

        if (empty($limit)) {
            $limit = 250;
        }

        $objFields = new CustomFields($numbers, 'add');

        if (empty($type)) {
            $type = 5;
        }

        if (empty($entity)) {
            $entity = 1;
        }

        $preResult = $objFields->getOneEntitiyId(API_CONTACTS);
        $preResult = $objFields->setArrayIdNewEntity($preResult);
        $customId = ['Empty']; // for default

        if (is_array($preResult) && !empty($preResult[0])) {
            $send = $objFields->generateCustomField($objFields->getEntity(), 10, $type, $entity, $preResult[0]);
            $result = $objFields->addFieldsToEntity($send); // add custom fields to all
            $objFields->setEntityId($objFields->setArrayIdNewEntity($result['_embedded']['items']));
            $customId = $objFields->getIdEntity();
            $objFields->setCustomFieldsId($customId);
            $objFields->setCustomFieldsValueId();
            $objFields->setGenerationRandomArrays(sizeof($objFields->getAllEntitiesId(API_CONTACTS)));
        }

        $step1 = $objFields->getAllEntitiesId(API_CONTACTS);
        $step2 = $objFields->setArrayIdNewEntity($step1);
        $step3 = $objFields->createResultCustomFieldsUpdateArray($step2, 5);
        $step = Control::getStepFromSizeArr($step3, $limit, 'update');
        $send = Control::devideArrays($step3, $limit, 'update');
        $result = Control::queryRestriction($send, $step, API_CONTACTS, 'update');

        return $result;
    }

    /**
     * добавление текстового поля
     * @return array
     */
    public function addText()
    {
        if (!empty($_POST['addtext']) &&
            (!empty($_POST['identity']) || !empty($_POST['identityTask'])) &&
            (!empty($_POST['entitytype']) || !empty($_POST['entitytype'])))
        {
            $objFields = new CustomFields(1, 'add');
            $result[] = 'addText: ' . $_POST['addtext'];
            $tryId = empty($_POST['identity']) ? (int) trim($_POST['identityTask']) : (int) trim($_POST['identity']);
            $entityType = empty($_POST['entitytype']) ? (int) trim($_POST['entitytypeTask']) : (int) trim($_POST['entitytype']);
            $result[] = 'entityId: ' . $tryId;
            $result[] = 'typeEntity: ' . $entityType;
            $input_text = $_POST['addtext'];
            $testCustomFields = $objFields->tryCustomFieldsAccount($tryId, $entityType);

            if ($testCustomFields['reply']) {
                $result[] = '<b>Not empty! Such an object exists.</b>';
                $id_fields = $objFields->getIdTextFields($testCustomFields['result_entity']);

                if (!empty(sizeof($id_fields))) {
                    // text field exist     -   not create field, add TEXT in exist field
                    $t_r = $objFields->generateCustomField($objFields->getEntity(), 1, 1, $entityType, $tryId, $input_text);
                    $objFields->setRandomArraysValues($t_r);
                    $t_r2 = $objFields->createResultCustomFieldsUpdateArray($id_fields);
                    $t_r3 = $objFields->entityOneQuickRequest($entityType, $t_r2);

                    if (!empty(sizeof($t_r3))) {
                        $result[] = 'Текстовое поле в сущности <b>'.$t_r3['_embedded']['items'][0]['id'].'</b> обновлено!';
                    }
                    else {
                        echo 'SEARCH EXIST TROUBLE';
                    }
                } else {
                    // text field not exist -   create field & add TEXT in this TEXT field
                    $t_r = $objFields->generateCustomField($objFields->getEntity(), 1, 1, $entityType, $tryId, $input_text);
                    $objFields->setRandomArraysValues($t_r);
                    $t_rn2 = $objFields->entityOneQuickRequest(4, $t_r);
                    $id_fields = [$t_rn2['_embedded']['items'][0]['id']];
                    $t_r2 = $objFields->createResultCustomFieldsUpdateArray($id_fields);
                    $t_r3 = $objFields->entityOneQuickRequest($entityType, $t_r2);
                    
                    if (!empty(sizeof($t_r3))) {
                        $result[] = 'Текстовое поле в сущности <b>'.$t_r3['_embedded']['items'][0]['id'].'</b> обновлено!';
                    }
                }
            } else {
                $result[] = '<b>Object empty!</b>';
            }
        } else {
            $result[] = 'Data not correct! Reinput!';
        }

        return $result;
    }

//    add note & incomming call
    public function addNote()
    {
        if (!empty($_POST['addtext']) && 
            (!empty($_POST['identity']) || !empty($_POST['identityTask'])) &&
            (!empty($_POST['entitytype']) || !empty($_POST['entitytype'])))
        {
            $input_text = $_POST['addtext'];
            $tryId = empty($_POST['identity']) ? (int) trim($_POST['identityTask']) : (int) trim($_POST['identity']);
            $entityType = empty($_POST['entitytype']) ? (int) trim($_POST['entitytypeTask']) : (int) trim($_POST['entitytype']);
            $obj_notes = new Notes(1, 'add', $entityType);
            $result = [];
            $t_note = 0;

            if ((int) $_POST['rbut'] === 1) {
                $t_note = 4;
            }

            $r = $obj_notes->setNoteObj($tryId, $t_note, $input_text);
            $t_addNote = $obj_notes->entityOneQuickRequest(5, $r);

            if (!empty($t_addNote['_embedded']['items'][0]['id'])) {
                $result[] = 'Примечание добавлено! ID: ' . $t_addNote['_embedded']['items'][0]['id'] .
                    ' | ID_ENTITY: <b>'.$tryId.'</b>';
            }
        } else {
            $result[] = 'Data not correct! Reinput!';
        }

        return $result;
    }

//    add note about calls in entity
    public function addPhone()
    {
        $t_note = 0;

        if ((int) $_POST['rbut'] === 2) {
            $t_note = 10;
        }

        $input_text = $_POST['addtext'];
        $tryId = (int) trim($_POST['identity']);
        $entityType = (int) trim($_POST['entitytype']);
        $obj_notes = new Notes(1, 'add', $entityType);
        $r = $obj_notes->setNoteObj($tryId, $t_note, $input_text);
        $r['add'][0] = array_merge($r['add'][0], $obj_notes->getCallParams());
        unset($r['add']['text']);
        $result['send'] = $r;
        $preResult = $obj_notes->entityOneQuickRequest(5, $r);
        $reault['reply'] = $preResult;

        foreach ($r['add'][0] as $key => $val) {
            $end_result[] = [$key => $val];
        }

        return $end_result;
    }

    /**
     * function add new Task to input entity by id
     * @param: $entityId, $id_user, $text_task, $end_time
     * @return Array
     */
    public function addTask()
    {
        if (isset($_POST['addtime'])) {
            $_POST['addtime'] = strtotime($_POST['addtime']);
        }

        $objTasks = new Tasks(1, 'add');
        $keysValuesAdd = [
            'element_id' => (int) trim($_POST['identityTask']),
            'element_type' => (int) trim($_POST['entitytypeTask']),
            'complete_till' => $_POST['addtime'],
            'task_type' => $_POST['typetask'],
            'text' => $_POST['addtexttask'],
            'responsible_user_id' => $_POST['iduser']
        ];
        $objTasks->setObjEntity($keysValuesAdd, 'add');
        $result = $objTasks->getObjEntity();
        $preResult = $objTasks->entityOneQuickRequest(7, $result);
        $result['request'] = $preResult;

        return $result;
    }

    /**end of the task
     * @return array
     */
    public function endTask()
    {
        $objTasks = new Tasks(1, 'update');
        $keysValuesUpdate = [
            'id' => (int) $_POST['idtask'],
            'is_completed' => 1,
            'updated_at' => time(),
            'text' => 'task of the end!'
        ];

        $objTasks->setObjEntity($keysValuesUpdate, 'update');
        $result = $objTasks->getObjEntity();
        $preResult = $objTasks->entityOneQuickRequest(7, $result);

        $result['post'] = $_POST;
        $result['request'] = $preResult;

        return $result;
    }
}
