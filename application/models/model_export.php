<?php

class Model_Export extends Model {

    public $entityNums;

    public function getData()
    {
		$data = [];
		$conn = $this->connect();
        
        if (!empty($_POST['testForm']) && ((bool) $conn['auth'] === true)) {
			$data['time'] = $conn['mess'];
        }

		if (empty($_POST['testForm']) && ((bool) $conn['auth'] === true)) {
			$data['time'] = $conn['mess'];
        }

		if (!empty($_POST['viewForm']) && $_POST['actionform'] == 'view') {
            // uncomment line down if needs writing logs in file
			// file_put_contents("./tracer.log", date("H:i:s", time() + 3600) . "--\n");
			$data['count'] = $this->countViewEntity();
		}

        // this point input numbers entity leds, contacts, companies
        $this->setInputEntityNums(509, 21983, 24473);
		$arrayNumEntity = $this->getInputNums();

        if (!empty($_POST['genForm'])) {
			$data['links'] = $this->genAllEntities();
		}

        if (!empty($_POST['genLeads'])) {
			$data['links'] = $this->genLinks('leads', $arrayNumEntity['leads']);
		}

        if (!empty($_POST['genContacts'])) {
			$data['links'] = $this->genLinks('contacts', $arrayNumEntity['contacts']);
		}

        if (!empty($_POST['genCompanies'])) {
			$data['links'] = $this->genLinks('companies', $arrayNumEntity['companies']);
		}

        return $data;
    }
	
    public function connect()
    {
		return Auth::tryAuth();
	}

    public function setInputEntityNums($leads, $contacts, $companies)
    {
		$this->entityNums = [
			'leads' => $leads,
			'contacts' => $contacts,
			'companies' => $companies
			];
	}

    public function getInputNums()
    {
		return $this->entityNums;
	}
	
    public function countViewEntity()
    {
		$link = $this->getLinkNameEntity();
        $count = [];

        foreach ($link as $key => $val) {
            $count[$key] = $val['count'];
        }

		return $count;
	}
	
    public function getLinkNameEntity()
    {
		$result = [];
		$entity = [];
        
        if ($_POST['entity'] == 'all') {
			for($i = 1; $i < 4; $i++) {
				$result[($i - 1)]['link_name'] = Entity::entTypeToApi($i);
				$entity = $this->getCountEntity($result[($i-1)]['link_name'][1]);
				$result[($i - 1)]['count'] = sizeof($entity);
				$result[($i - 1)]['entity'] = $entity;
				unset($entity);
			}
		} else {
			$result[0]['link_name'] = Entity::entTypeToApi($_POST['entity']);
			$entity = $this->getCountEntity($result[0]['link_name'][1]);
			$result[0]['count'] = $entity;
			unset($entity);
		}
		
		return $result;
	}
	
    public function getCountEntity($link)
    {
		return Entity::getAllEntitiesId($link);
	}
	
    public function genAllEntities()
    {
		if (!empty($this->entityNums)) {
			$entities = $this->getInputNums();
		} else { // this is default counts of entites:
			$entities = [
				'leads' => 12019,
				'contacts' => 12016,
				'companies' => 12015
				];
		}
        
        $allEntities = [];
        
        foreach ($entities as $key => $val) { 
			$allEntities[$key] = $this->genLinks($key, $val);
		}
        
        return $allEntities;
	}
	
    public function genLinks($entity, $number)
    {
		$arrayLinks = [];
		$count = ($number - ($number % 500)) / 500;

        if ($number % 500 > 0) {
			$count++;
		}

		for ($i = 1;$i <= $count; $i++) {
			if ($entity == 'leads') {
                $arrayLinks[] = HT.SUBDOMAINE.DOM . 'ajax/' . $entity .
                    '/export/?&filter[pipe][157689]=1&pipeline_id=157689&export_type=excel&ELEMENT_COUNT=500&PAGEN_1=' . $i;
			} elseif ($entity == 'contacts') {
                $arrayLinks[] = HT.SUBDOMAINE.DOM . 'ajax/' . $entity .
                    '/export/?&element_type=2&export_type=excel&ELEMENT_COUNT=500&PAGEN_1=' . $i;
			} else {
                $arrayLinks[] = HT.SUBDOMAINE.DOM . 'ajax/' . $entity .
                    '/export/?&element_type=3&export_type=excel&ELEMENT_COUNT=500&PAGEN_1=' . $i;
			}
		}

        return $arrayLinks;
	}
	
}
