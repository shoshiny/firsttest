<?php

class Model_G1widget extends Model {

    /** create content and get result
     * @return array
     */
    public function getData()
    {
		$path = 'widget';

        if (!empty($_POST['wpath']) &&
			is_dir($_POST['wpath']) &&
			is_file($_POST['wpath'] . '/manifest.json')) {
			$path = $_POST['wpath'];
		}

		$file = $this->setResourceFile($path);
        $main = $this->getFile($file);
        $main['wpath'] = $path;
		$main['secret_key'] = $this->getKey($file, 'secret_key');

        if (!empty($_POST['save'])) {
            $main['save'] = $this->saveNewManifest($main['new_file'], $path);
        }

        if (!empty($_POST['savekey']) && !empty($_POST['skey'])) {
            $main['savekey'] = $this->saveNewKeyValue($file, ['secret_key' => $_POST['skey']], $path);
        }

        if (!empty($_POST['create'])) {
			$widgetPath = '';

			if (!empty($_POST['wpath'])) {
				$widgetPath = $_POST['wpath'];
			}

            $main['create'] = $this->getDataForZipWidget($widgetPath);
            $main['list'] = $this->getNameFiles($main['create']['path'], $path);
            $main['zip'] = $this->getWidget($main['list']['files'], $path);
        }

        return $main;
    }

    /**
     * @return array $result
     */
    public function setResourceFile($path)
    {
        $fileName = $path . '/manifest.json';

        if (file_exists($fileName)) {
            $result['file'] = file($fileName);
            $result[] = 'file exist!';
        } else {
            $result[] = 'file not exist!';
        }

        return $result;
    }

    /**
     * @param $file
     * @return array $version
     */
    public function getFile($file)
    {
        if (is_array($file['file'])) {
            $newFile = [];

            foreach ($file['file'] as $fileRow) {
                if (strstr($fileRow, '"version"')) {
                    $verArray = explode('"', $fileRow);
                    $version['ver'] = $verArray[3];
                    $newVers = $this->getNewVersion($version['ver']);
                    $version['new'] = $newVers;

                    if (!empty($_POST['version']) && ($_POST['version'] !== $version['ver'])) {
                        $version['new'] = $_POST['version'];
                    }

                    $verArray[3] = $version['new'];
                    $newFileRow = implode('"', $verArray);
                    $version['row'] = $newFileRow;
                    $newFile[] = $newFileRow;
                } else {
                    $newFile[] = $fileRow;
                }
            }
            $version['new_file'] = $newFile;
        } else {
            $version[] = 'Doesn\'t have info about version of manifest file, or not read in file!';
        }

        return $version;
    }
	
    public function getKey($file, $key)
    {
		foreach ($file['file'] as $fileRow) {
			if (strstr($fileRow, $key)) {
				$verArray = explode('"', $fileRow);
				$result = $verArray[3];
				break;
			}
		}

		return $result;
	}


    public function saveNewKeyValue($file, $new, $path)
    {
		foreach ($new as $key => $val) {
			if (is_array($file['file'])) {
				$newFile = [];

                foreach ($file['file'] as $fileRow) {
					if (strstr($fileRow, '"' . $key . '"')) {
						$verArray = explode('"', $fileRow);
						$version[$key] = $verArray[3];
						$version[$key . '_new'] = $val;
						$verArray[3] = $version[$key . '_new'];
						$newFileRow = implode('"', $verArray);
						$version['row'] = $newFileRow;
						$newFile[] = $newFileRow;
					} else {
						$newFile[] = $fileRow;
					}
				}
				$version['new_file'] = $newFile;
			} else {
				$version[] = 'Doesn\'t have info about version of manifest file, or not read in file!';
			}			
        }

        $file = $version['new_file'];

        return $this->saveNewManifest($file, $path);
    }

    /**
     * @param $ver
     * @return string $result
     */
    public function getNewVersion($ver)
    {
        $ver_arr = explode('.', $ver);
        $ver_count = sizeof($ver_arr) - 1;
        $ver_arr[$ver_count]++;
        $result = implode('.', $ver_arr);

        return $result;
    }

    /**
     * @param $file
     * @param $source
     * @param $arr
     * @return array $result
     */
    public function checkFile($file, $source, $arr)
    {
        $newFile = file($file);

        if (sizeof(array_diff($newFile, $source)) !== 0) {
            $result[] = $arr[0];
        } else {
            $result[] = $arr[1];
        }

        return $result;
    }

    /**
     * @param $file
     * @return array
     */
    public function saveNewManifest($file, $path)
    {
        $inputFile = $path . '/manifest.json';
        $newFile = fopen($inputFile, 'w');
        fwrite($newFile, implode($file));
        fclose($newFile);

        return $this->checkFile($inputFile, $file, ['save error', 'save done']);
    }

    /**
     * @param $files
     * @return array $result
     */
    public function getWidget($files, $path)
    {
        $zip = new ZipArchive();

        if (!empty($_POST['namewidg'])) {
            $zip_name = $path . '/' . $_POST['namewidg'].'.zip';
        } else {
            $zip_name = $path . '/' . 'widget.zip';
        }

        if ($zip->open($zip_name, ZipArchive::CREATE) !== true) {
            $result['errors'][] = 'ZIP creation failed';
        }

        foreach ($files as $file) {
            $zip->addFile($file, str_replace($path . '/' , '', $file));
        }

        $zip->close();

        if (file_exists($zip_name)) {
            /* uncommit this if you need to save zip file into browser
            header('Content-type: application/zip');
            header('Content-Disposition: attachment; filename="'.$zip_name.'"');
            readfile($zip_name);
            // удаляем zip файл если он существует и не нужен
            unlink($zip_name);
            */
            $result[] = 'done, zip building';
        } else {
            $result[] = 'Undone!';
        }

        return $result;
    }

    /**
     * @return string $result
     */
    public function getDataForZipWidget($widgetPath)
    {
        $result['path'] = $this->scanDir($widgetPath, 'scanCall');

        return $result;
    }

    /**
     * @param $dir
     * @param null $call
     * @return array $result
     */
    public function scanDir($dir, $call = null)
    {
		if (empty($dir) || !is_dir($dir)) {
			$dir = 'widget';
		}
        
        $result = [];

        if ($d = opendir($dir)) {
            while ($fname = readdir($d)) {
                if ($fname === '.' || $fname === '..' || strstr($fname, '.zip')) {
                    continue;
                } else {
                    // Передать путь файла в callback-функцию
                    if ($call !== null && is_callable("Model_G1widget::$call")) {
                        if (is_file($dir.DIRECTORY_SEPARATOR.$fname))
                            $result[] = call_user_func("Model_G1widget::$call", $dir . DIRECTORY_SEPARATOR . $fname);
                        else
                            $result[] = call_user_func("Model_G1widget::$call", $dir . DIRECTORY_SEPARATOR . $fname);
                    }
                }
                
                if (is_dir($dir . DIRECTORY_SEPARATOR . $fname)) {
                    $result[] = $this->scanDir($dir . DIRECTORY_SEPARATOR . $fname, $call);
                }
            }
            closedir($d);
        }

        return $result;
    }

    /**
     * @param $fname
     * @return string $fname
     */
    public function scanCall($fname)
    {
        $fname = explode('\\', $fname);
        $t = sizeof($fname);

        return $fname[($t - 1)];
    }

    /**
     * @param $arr
     * @return array $result
     */
    public function getNameFiles($arr, $path)
    {
        $temp = '';
        foreach ($arr as $first) {
            if (!is_array($first)) {
                if (strstr($first, '.')) {
                    $temp = '';
                    $result['files'][] = $path . '/' . $first;
                } else {
                    $result['dirs'][] = $path . '/' . $first . '/';
                    $temp = $first . '/';
                }
            } else {
                    foreach ($first as $file) {
                        $result['files'][] = $path . '/' .$temp. $file;
                    }
                }
            }

        return $result;
    }
}
