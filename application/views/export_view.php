<h3>Export all entities in CSV:</h3>
<div class="tsk">
<?php
$server = '';
$subdom = '';
$login = '';
$keyapi = '';
$checkedSaas = '';
$checkedAmo = '';
$checkedLeads = '';
$checkedContacts = '';
$checkedCompanies = '';
$checkedAll = '';
$checkedView = '';
$checkedExport = '';
$checkedTest = '';
$check = 'checked';

if ((defined('HT') &&
    defined('DOM') &&
    defined('SUBDOMAINE') &&
    defined('LOGIN') &&
    defined('HASH')))
{
	$server = '[url: ' . HT.SUBDOMAINE.DOM . ']';
	$subdom = '[' . SUBDOMAINE . ']';
	$login = '[' . LOGIN . ']';
	$keyapi = '[' . HASH . ']';
}

if (!empty($_POST['send_form'])) {
	$checkedSaas = ((bool) $_POST['saas']) ? $check : '';
	$checkedAmo = !((bool) $_POST['saas']) ? $check : '';
	$checkedLeads = ((int) $_POST['entity'] === 2) ? $check : '';
	$checkedContacts = ((int) $_POST['entity'] === 1) ? $check : '';
	$checkedCompanies = ((int) $_POST['entity'] === 3) ? $check : '';
	$checkedAll = ((string) $_POST['entity'] === 'all') ? $check : '';
	$checkedView = ((string) $_POST['actionform'] === 'view') ? $check : '';
	$checkedExport = ((string) $_POST['actionform'] === 'export') ? $check : '';
	$checkedTest = ((string) $_POST['actionform'] === 'test') ? $check : '';
} else {
	$checkedSaas = $check;
	$checkedAll = $check;
	$checkedTest = $check;
}

?>

<div class="container small"><b>Addres:</b> <?php echo $server; ?></div>
<div class="container small">&nbsp;</div>
<form action="./export" method="post">
<input type="hidden" name="send_form" value="this form i send">
<div class="container ">
        <div class="row">
            <div class="col-sm">
	<div><b>Subdomain:</b><br><span class="small"><?php echo $subdom; ?></span></div>
	<div><input type="text" value="<?php if (!empty($_POST['addsubdom'])) {echo $_POST['addsubdom'];} ?>" name="addsubdom" class="form-control form-control-sm" placeholder="Input Subdomain"></div>
			</div>
            <div class="col-sm">
	<div><b>Login:</b><br><?php echo $login; ?></div>
	<div><input type="text" value="<?php if (!empty($_POST['addlogin'])) {echo $_POST['addlogin'];} ?>" name="addlogin" class="form-control form-control-sm" placeholder="Input Login: example@exemple.com"></div>
			</div>
            <div class="col-sm">
	<div><b>API Key:</b><br><?php echo $keyapi; ?></div>
	<div><input type="text" value="<?php if (!empty($_POST['addkeyapi'])) {echo $_POST['addkeyapi'];} ?>" name="addkeyapi" class="form-control form-control-sm" placeholder="Input Key API"></div>
			</div>
		</div>
		<div class="container small">&nbsp;</div>
        <div class="row">
            <div class="col-sm">
				Server:<br>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="saas" value="true" id="saastrue" <?php echo $checkedSaas; ?>>
					<label for="saastrue">SAAS</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="saas" value="" id="saasfalse" <?php echo $checkedAmo; ?>>
					<label for="saasfalse">АМОСRМ.RU</label>
				</div>
            </div>
            <div class="col-sm">
				Entity:<br>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="entity" value="all" id="entityall" <?php echo $checkedAll; ?>>
					<label for="entityall">All</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="entity" value="2" id="leads" <?php echo $checkedLeads; ?>>
					<label for="leads">Leads</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="entity" value="1" id="contacts" <?php echo $checkedContacts; ?>>
					<label for="contacts">Contacts</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="entity" value="3" id="companies" <?php echo $checkedCompanies; ?>>
					<label for="companies">Companies</label>
				</div>
            </div>
            <div class="col-sm">
				Action:<br>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="actionform" value="export" id="action_export" <?php echo $checkedExport; ?>>
					<label for="action_export">Export CSV</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="actionform" value="test" id="action_test" <?php echo $checkedTest; ?>>
					<label for="action_test">Test Connection</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="actionform" value="view" id="action_view_count" <?php echo $checkedView; ?>>
					<label for="action_view_count">View Count Enties</label>
				</div>
            </div>
        </div>
		<hr>
        <div class="row">
            <div class="col-sm">
            </div>
            <div class="col-sm">
            </div>
            <div class="col-sm">
                <input type="submit" name="sendForm" value="Export" class="btn btn-primary btn-sm">
                <input type="submit" name="testForm" value="Test" class="btn btn-primary btn-sm">
                <input type="submit" name="viewForm" value="View" class="btn btn-primary btn-sm"><br><br>
                <input type="submit" name="genForm" value="Generate All" class="btn btn-primary btn-sm">
                <input type="submit" name="genLeads" value="Generate Leads" class="btn btn-primary btn-sm"><br><br>
                <input type="submit" name="genContacts" value="Generate Contacts" class="btn btn-primary btn-sm">
                <input type="submit" name="genCompanies" value="Generate Companies" class="btn btn-primary btn-sm">
            </div>
        </div>
</div>
</form>
</div>
<?php
if (!empty($_POST['send_form'])) {
?>
<br>
<div class="tsk">
	<div class="container">
<pre>
<?php

if (isset($data['time'])) {
	echo $data['time'];
}

?>


</pre>
<div class="container small small">
<script type="text/javascript">
var links = [
<?php
foreach ($data['links'] as $key => $val) {
	if (is_array($val) && !is_string($val)) {
		foreach ($val as $key_entity => $val_entity) {
			echo '"'.$val_entity."\",\n";
			;
		}
	} else {
		echo '"'.$val."\",\n";
	}
}
echo '];'."\n"."\n";
?>
var count_interval = 0,
	time_set,
	startid = 0;
	// my_win = null,
var cnt_def = 45,
	cnt = cnt_def;
	time_clock;
function test() {
	var max_count = links.length;
	if (count_interval == max_count) {
		clearInterval(time_set);
		console.log('Script End!');
		// break;
	} else {
		console.log(count_interval + ": " + links[count_interval] + "\n");
	// window.location.href = links[count_interval];
		/* if (my_win != -1) {
			my_win = window.open(links[count_interval]);
		} */
		window.open(links[count_interval]);
		// clearInterval(time_set);
		// alert(my_win); - opera crash
		// console.log(my_win);
		// if (my_win.closed == true) {
		console.log(count_interval + " : " + links[count_interval] + " - download! \n");
			// time_set = setInterval("test()", 10000);
/*
		my_win.onbeforeunload = function() {
			console.log(count_interval + " : " + links[count_interval] + " - download! \n");
		}*/
		var timetotal = ((max_count - (count_interval - 1)) * cnt_def) / 3600;
		$('#progress b').text((count_interval + 1) + ' as ' + max_count + '(' + Math.round((((count_interval + 1) / max_count) * 100)) + '%) ~~ ' + timetotal + ' hours' + ' // ' + cnt_def);
		count_interval++;
		// my_win = -1
		// }
	}
}

function get_link() {
	var t = links.length;
	
	if ($('#startid').val() != '' && $('#startid').val() > 0) {
		startid = $('#startid').val() - 1;
		count_interval = startid;
	}

	console.log('Total count links: '+ (t - startid));
//	for(var k = 0; k < t; k++) {
//		console.log(k + ": " + "\n");
	time_set = setInterval("test()", (cnt_def*1000));
	get_clock();
//	}
/* 	if (startid != '' && startid > 0) {
		
	} */

	if (count_interval == t) {
		clearInterval(time_set);
		clearInterval(time_clock);
	} else {
		console.log(count_interval + ":\n");
	}

	return console.log(links.length);
}

function clock(t) {
	$('#dnf').html(cnt);
	cnt--;
	if (cnt == -1) {
		cnt = cnt_def;
	}
}

function get_clock() {
	time_clock = setInterval("clock()", 1000);
}
</script>

<?php
/*
                <input type="submit" name="genForm" value="Generate All" class="btn btn-primary btn-sm">
                <input type="submit" name="genLeads" value="Generate Leads" class="btn btn-primary btn-sm"><br><br>
                <input type="submit" name="genContacts" value="Generate Contacts" class="btn btn-primary btn-sm">
                <input type="submit" name="genCompanies" value="Generate Companies" class="btn btn-primary btn-sm">
				*/
$old = 'Generate ';
$button_name = 'Get ';
if (!empty($_POST['genForm'])) {
	$button_name = str_replace($old, $button_name, $_POST['genForm']);
} elseif (!empty($_POST['genLeads'])) {
	$button_name = str_replace($old, $button_name, $_POST['genLeads']);
} elseif (!empty($_POST['genContacts'])) {
	$button_name = str_replace($old, $button_name, $_POST['genContacts']);
} elseif (!empty($_POST['genCompanies'])) {
	$button_name = str_replace($old, $button_name, $_POST['genCompanies']);
}
?>
<input class="control-form" name="startid" id="startid" value="" placeholder="input number of start">
<input type="button" onclick="get_link();return false;" name="getAll" value="<?php echo $button_name; ?>" class="btn btn-primary btn-sm"> <b>time to download file: </b><b id="dnf">0</b> <div id="progress">Progress: <b></b></div>
<?php /*
<input type="button" onclick="get_link();return false;" name="getLeads" value="Get Leads" class="btn btn-primary btn-sm"> 
<input type="button" onclick="get_link();return false;" name="getContacts" value="Get Contacts" class="btn btn-primary btn-sm"> 
<input type="button" onclick="get_link();return false;" name="getCompanies" value="Get Companies" class="btn btn-primary btn-sm">
*/ ?>
		</div>
	</div>
</div>
<?php
}