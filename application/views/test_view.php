<?php
/*
this file for experiment & test ideas
for fast test to work code on the web-server
access by: ://web-serv/test
*/
?>
<div class="container">Тест<hr>
<?php
	foreach ($data as $key => $val) {
		echo $key .' => '. $val .'<br>';
	}
?>
	<hr>
	<pre>
<?php
	print_r($data);
	$l = 4;
	$k = 12;
	$t = ($l === $k) ? 'test' : 'TEST';
	echo '<hr>'.$t;
	$t = 4;
	echo '<hr>';
	if (in_array($t, [1, 2, 3, 4, 5])) {
		echo 'YEEEE';
	} else {
		echo 'NOOO';
	}
	?>
	<hr>
<?php
$array1 = array("a" => "green", "red", "blue", "red");
$array2 = array("b" => "green", "yellow", "c" => "red", "white");
?>
	</pre>
	<div class="row">
		<div class="col-sm">
			<pre>
<?php
echo '$array1<br>';
print_r($array1);
?>
			</pre>
		</div>
		<div class="col-sm">
			<pre>
<?php
echo '$array2<br>';
print_r($array2);
?>
			</pre>
		</div>
	</div>
	<div class="row">
		<div class="col-sm">
			<pre>
<?php
$result = array_diff($array1, $array2);
echo 'array_diff($array1, $array2):<br>';
print_r($result);
?>
			</pre>
		</div>
		<div class="col-sm">
			<pre>
<?php
$result = array_diff($array2, $array1);
echo 'array_diff($array2, $array1):<br>';
print_r($result);
?>
			</pre>
		</div>
	</div>
	<div>
<?php

// list($items, $current_page, $max_page, $count) = $this->get_items($event, $next_page);
?>
	</div><hr>
	<div class="container"><pre>
	<?php
$array1 = [
			0 => ["id" => "12345"],
			1 => ["id" => "12346"],
			2 => ["id" => "12347"],
			3 => ["id" => "12348"],
			4 => ["id" => "12349"],
			5 => ["id" => "12350"]
			];
			$array1 = [$array1];
print_r($array1);

$result = [];
foreach ($array1 as $key => $value) {
	$result = array_merge($result, $value);
}
echo '<div class="alert-info">$result = [];<br>foreach ($array1 as $value) {<br>$result = array_merge($result, $value);<br>}</div>';
print_r($result);
?></pre>
<hr>
<pre>
<?php
$t['test'] = function($c) {
	$obj = null;
	$obj = $c;
	return $obj;
};

?>
<hr>
<hr>
<?php

$entry = array(
             0 => 'foo',
             1 => false,
             2 => -1,
             3 => null,
             4 => '',
             5 => '0',
             6 => 0,
             7 => 'dghvbxxfdghsd',
             8 => 567485,
             8 => "afgttwe 23734",
          );

print_r(array_filter($entry));
?>
<br>--------------------------------------<br>
<?php
$marr = [
    'file_19' => [0 => null]
];
	if (array_key_exists('file_19', $marr)) {
		echo 'FILE!';
	}
?>
<hr>
<?php
for ($i = 0; $i <= 10; $i++) {
	if ($i === 5) {
		break;
	} else {
		echo $i."<br>";
	}
}
?>
</pre>
<hr>
<?php
$arr = [
    'testep1' => 'val1',
    'test2' => 'val2'
];
foreach ($arr as $key => &$val) {
	echo $key.' = '.$val.'<br>';
}
unset($val);
?><hr>
<?php
$test23 = 1253;

if ($test23 === false) {
	echo 'Not true';
} else {
	echo 'Is true';
}
?>
	</div>
</div>