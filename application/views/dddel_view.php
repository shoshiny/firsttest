<?php
/*
Содержимое этого файла (скрипт часть) нужно выполнять в консоле,
на портале, при большом (огрромном) количестве сущностей.
Зафиксировано тут на отдельной странице, чтобы не забыть.
*/
?>
Начинается работа скрипта:
<script type="text/javascript">
    function del(num) {
        var config = {
            domain: '<?php echo SUBDOMAINE; ?>',
            typeEntity: num
        };
        var entities_map = {
            2: 'leads',
            3: 'companies',
            1: 'contacts',
        };

        while (true) {
            try {
                var listRes = $.ajax({
                        url: 'https://' + config.domain + '.amocrm.ru/private/api/v2/json' +
                        entities_map[config.typeEntity] + '/list/',
                        async:false
                    });
                var items = listRes.responseJSON.response[entities_map[config.typeEntity]];
            } catch (err) {
                console.error(err);
                break;
            }

            if (!items || items.length === 0) {
                break;
            }

            var ids = _.pluck(items, 'id');
            try {
                var deleteRes = $.ajax({
                    url: 'https://' + config.domain + '.amocrm.ru/ajax/' +
                    entities_map[config.typeEntity] + '/multiple/delete',
                    type: 'POST',
                    data: {
                        ID: ids
                    },
                    async: false
                });
            } catch(err) {
                console.error(err);
            }
        }
    }
</script>
<a href="#" onclick="del(1);return false;">delete contacts</a> /
<a href="#" onclick="del(2);return false;">delete leads</a> /
<a href="#" onclick="del(3);return false;">delete companies</a> /
delete customers<br>
<div class="tsk">
    <div class="result">
    <?php

    foreach ($data as $key => $val) {
        if (is_string($val[1])) {
            echo ($key + 1) . ') i => ' . $val . '<br>';
        } elseif (is_array($val[1]) && (!isset($val[1]['_links']) && isset($val[1][0]))) {
            echo ($key + 1) . ') ei ' . $val[0] . ' => ' . sizeof($val[1]) . '<br>';
        } else {
            echo ($key + 1) . ') e ' . $val[0] . ' => 0' . '<br>';
        }
    }
    ?>
    </div>
</div>