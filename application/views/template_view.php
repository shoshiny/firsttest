<?php
/*
This is basical template for all web-app.
*/
?>
<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $this->getPageTitle(); ?></title>
    <script src="/js/jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="top-menu">
    <a href='/'>HOME</a> / <a href='/grade1'>Grade 1</a> / <a href='/g1widget'>Grade 1 - Widget</a> / <a href='/export'>Export</a>
</div>
<hr>
<div class="container">
    <?php require_once 'application/views/' . $contentView; ?>
</div>
</body>
</html>