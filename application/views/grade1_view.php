<?php
$server = '';
$subdom = '';
$login = '';
$keyapi = '';
$checkedSaas = '';
$checkedAmo = '';
$checkedLeads = '';
$checkedContacts = '';
$checkedCompanies = '';
$checkedAll = '';
$checkedView = '';
$checkedExport = '';
$checkedTest = '';
$check = 'checked';

if ((defined('HTTPS') && defined('DOM') && defined('SUBDOMAINE') && defined('LOGIN') && defined('HASH')))
{
	$server = '[url: ' . HTTPS.SUBDOMAINE.DOM . ']';
	$subdom = '[' . SUBDOMAINE . ']';
	$login = '[' . LOGIN . ']';
	$keyapi = '[' . HASH . ']';
}

if (!empty($_POST['send_form'])) {
	$checkedSaas = ((bool) $_POST['saas']) ? $check : '';
	$checkedAmo = !((bool) $_POST['saas']) ? $check : '';
} else {
	$checkedAmo = $check;
}

if (!empty($data['err'])) {
    echo '<div class="alert alert-danger" role="alert">' . $data['err'] . '</div>';
}
?>
<form action="./grade1" method="post">
<input type="hidden" name="send_form" value="i send this form">
<div class="tsk">
        <div class="row">
            <div class="col-sm">
	<div><b>Subdomain:</b><br><span class="small"><?php echo $subdom; ?></span></div>
	<div><input type="text" value="<?php if (!empty($_POST['addsubdom'])) {echo $_POST['addsubdom'];} ?>" name="addsubdom" class="form-control form-control-sm" placeholder="Input Subdomain"></div>
			</div>
            <div class="col-sm">
	<div><b>Login:</b><br><?php echo $login; ?></div>
	<div><input type="text" value="<?php if (!empty($_POST['addlogin'])) {echo $_POST['addlogin'];} ?>" name="addlogin" class="form-control form-control-sm" placeholder="Input Login: example@exemple.com"></div>
			</div>
            <div class="col-sm">
	<div><b>API Key:</b><br><?php echo $keyapi; ?></div>
	<div><input type="text" value="<?php if (!empty($_POST['addkeyapi'])) {echo $_POST['addkeyapi'];} ?>" name="addkeyapi" class="form-control form-control-sm" placeholder="Input Key API"></div>
			</div>
		</div>
		<div class="container small">&nbsp;</div>
        <div class="row">
            <div class="col-sm">
				Server:<br>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="saas" value="true" id="saastrue" <?php echo $checkedSaas; ?>>
					<label for="saastrue">local</label>
				</div>
                <div class="form-check">
					<input class="form-check-input" type="radio" name="saas" value="" id="saasfalse" <?php echo $checkedAmo; ?>>
					<label for="saasfalse">АМОСRМ.RU</label>
				</div>
            </div>
        </div>
	
</div>

<div class="tsk">
    <div class="container small">
            <div class="row">
                <div class="col-sm">Задайте количество сущностей от 0 до 10000:</div>
            </div>
            <div class="row">
                <div class="col-sm"><input type="text" name="number" class="form-control form-control-sm" placeholder="Number of Entity"></div>
                <div class="col-sm"><input type="submit" name="send" value="Создать сущности и мультиполя контактам" class="btn btn-primary btn-sm">
				<input type="submit" name="sendonly" value="Создать сущности " class="btn btn-primary btn-sm"></div>
            </div>
<hr>


<?php
if (!empty($data) && is_array($data) && (isset($_POST['send']) || isset($_POST['sendonly']))) {
?>
<div><br>~~ \Results HERE/ ~~begin..<br>&nbsp;</div>
<div class="result">
<?php
    foreach ($data as $key => $val) {
        if (is_string($val[1])) {
            echo ($key+1).') i => '.$val.'<br>';
        } elseif (is_array($val[1]) && (!isset($val[1]['_links']) && isset($val[1][0]))) {
            echo ($key+1).') ei '.$val[0].' => '.sizeof($val[1]).'<br>';
        } else {
            echo ($key+1).') e '.$val[0].' => 0'.'<br>';
        }
    }
?>
</div>
<div><br>~~ /Results HERE\~~ end..<br>&nbsp;</div>
<?php } ?>
    </div>
</div>
<div class="tsk">
    <div class="container small">
        <div class="row">
            <div class="col-sm">Установка значения доп.поля типа ТЕКСТ по ID сущности.</div>
        </div>
        <div class="row">
            <div class="col-sm">ID сущности:<br><input type="text" name="identity" class="form-control form-control-sm" placeholder="ID Entity"></div>
            <div class="col-sm">Тип сущности:<br>
                <select name="entitytype" class="form-control form-control-sm">
                <option value="1">Контакт</option>
                <option value="2">Сделка</option>
                <option value="3">Компания</option>
                <option value="12">Покупатель</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">Текст для поля / текст примечания:<br>
                <input type="text" name="addtext" class="form-control form-control-sm" placeholder="Text fields / Text remark"></div>
            <div class="col-sm"><br>
                <input type="submit" name="addText" value="Добавить значение" class="btn btn-primary btn-sm"></div>
        </div><hr>
        <div class="row">
            <div class="col-sm">Добавление примечания или входящего звонка в сущность по ее ID.</div>
        </div>
        <div class="row">
            <div class="col-sm"><br>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rbut" value="1" id="plainnote" checked>
                    <label for="plainnote">обычное примечание</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="rbut" value="2" id="incomingcall">
                    <label for="incomingcall">входящий звонок</label>
                </div>
            </div>
            <div class="col-sm"><br>
                <input type="submit" name="addNote" value="Добавить примечание" class="btn btn-primary btn-sm">
				<input type="submit" name="addNote100" value="+100 примечаний" class="btn btn-primary btn-sm">
				<input type="submit" name="addNote200" value="+200 примечаний" class="btn btn-primary btn-sm">
                <br>введите ID сущности, <br>и примечание выше
            </div>
        </div>
    <hr>
    <?php if (!empty($_POST['addText']) || !empty($_POST['addNote']) ||
                !empty($_POST['addNote100']) || !empty($_POST['addNote200'])) { ?>
    <div><br>~~ \Results HERE/ ~~begin..<br>&nbsp;</div>
        <?php
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        ?>
    <div><br>~~ /Results HERE\ ~~<br>&nbsp;</div>
    <?php } ?>
    </div>
</div>
<div class="tsk">
    <div class="container small">
        <div class="row">
            <div class="col-sm">
                Нажмите, чтобы добавить задачу в элемент указанной сущности.<br>
                Параметры, которые можно установить задаче:
            </div>
        </div>
        <div class="row">
            <div class="col-sm">ID сущности:<br><input type="text" name="identityTask" class="form-control form-control-sm" placeholder="ID Entity"></div>
            <div class="col-sm">Тип сущности:<br>
                <select name="entitytypeTask" class="form-control form-control-sm">
                    <option value="1">Контакт</option>
                    <option value="2">Сделка</option>
                    <option value="3">Компания</option>
                    <option value="12">Покупатель</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                Текст задачи:<br>
                <input type="text" value="" name="addtexttask" class="form-control form-control-sm" placeholder="Text task">
            </div>
            <div class="col-sm">
                Тип задачи:<br>
                <select name="typetask" class="form-control form-control-sm">
                    <option value="1">Звонок</option>
                    <option value="2">Встреча</option>
                    <option value="3">Написать письмо</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
                ID ответственного:<br>
                <input type="text" value="" name="iduser" class="form-control form-control-sm" placeholder="ID user">
            </div>
            <div class="col-sm">
                Завершить до даты и времени:<br>
                <input type="text" value="<?php echo date("d-m-Y H:m", time()+3600*24);?>" name="addtime" class="form-control form-control-sm" placeholder="<?php echo date("d-m-Y H:m", time()+3600*24);?> (пример: завтра)">
            </div>
        </div>
        <div class="row">
            <div class="col-sm">
            </div>
            <div class="col-sm"><br>
                <input type="submit" name="addTask" value="Добавить задачу" class="btn btn-primary btn-sm">&nbsp
                <input type="submit" name="add500" value="+500 задач" class="btn btn-primary btn-sm">&nbsp
                <input type="submit" name="add100" value="+100 задач" class="btn btn-primary btn-sm">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-sm">
                Введите ID задачи для завершения:<br>
                <input type="text" value="" name="idtask" class="form-control form-control-sm" placeholder="ID task">
            </div>
            <div class="col-sm"><br>
                <input type="submit" name="endTask" value="Завершить задачу" class="btn btn-primary btn-sm">
            </div>
        </div>
    </form>
    <hr>
    <?php if (!empty($_POST['addTask']) || !empty($_POST['add100']) ||
                !empty($_POST['add500']) || !empty($_POST['endTask'])) { ?>
        <div><br>~~ \Results HERE/ ~~begin..<br>&nbsp;</div>
        <?php
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        ?>
        <div><br>~~ /Results HERE\ ~~<br>&nbsp;</div>
    <?php } ?>
    </div>
</div>