<?php

class Request {

    public $_count;
 
    /**
     * send request to API and get reply
     * @param $apiLink
     * @param null $request
     * @param null $limitsOffset
     * @param null $id
     * @return array $response
     */
    public function requestApi($apiLink, $request = null, $limitsOffset = null, $id = null)
    {
        $addLink = '';
        
        if (!empty($limitsOffset)) {
            $addLink = '/?limit_rows=500&limit_offset=' . $limitsOffset;
        }

        if (!empty($id)) {
            $addLink = '/?id=' . $id;
        }

        if ($apiLink === API_ACCOUNT) {
            $addLink = '?with=custom_fields';
        }
		
        $link = HTTPS . SUBDOMAINE . '.' . DOM . '/' . $apiLink . $addLink;

        // Необходимо инициировать запрос к серверу. Воспользуемся библиотекой cURL (поставляется в составе PHP).
        // Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
        $curl = curl_init(); // Сохраняем дескриптор сеанса cURL
        // Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        // для авторизации и запросов

        if (isset($request) && is_array($request)) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
            $httpHeader[] = 'Content-Type: application/json';
            curl_setopt($curl, CURLOPT_HTTPHEADER, $httpHeader);
        }

        curl_setopt($curl, CURLOPT_HEADER, false);

        try {
            $cookiePath = ROOT . '/../cookie';

            if (is_dir(ROOT . '/../cookie')) {
                curl_setopt($curl, CURLOPT_COOKIEFILE, $cookiePath . '/cookie.txt');
                curl_setopt($curl, CURLOPT_COOKIEJAR, $cookiePath . '/cookie.txt');
            } else {
                throw new Exception('Message: doesn\'t save cookie for Authorisation.', 1);
            }

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

            // для сделок
            if (strstr($apiLink, 'leads') && empty($request)) {
                // Вы также можете передать дополнительный HTTP-заголовок IF-MODIFIED-SINCE,
                // в котором указывается дата в формате D, d M Y H:i:s. При передаче этого
                // заголовка будут возвращены сделки, изменённые позже этой даты.
                unset($httpheader);
                $httpheader[] = 'IF-MODIFIED-SINCE: Mon, 01 Aug 2013 07:07:23';
                curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader); // модификация запроса
                // Выполняем запрос к серверу.
            }

            $out = curl_exec($curl); // Инициируем запрос к API и сохраняем ответ в переменную
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE); // Получим HTTP-код ответа сервера
            curl_close($curl); // Завершаем сеанс cURL

            // Теперь можно обработать ответ, полученный от сервера.
            $code = (int) $code;
            $errors = [
                301 => 'Moved permanently',
                400 => 'Bad request',
                401 => 'Unauthorized',
                403 => 'Forbidden',
                404 => 'Not found',
                500 => 'Internal server error',
                502 => 'Bad gateway',
                503 => 'Service unavailable'
            ];

            try {
                // Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
                if ($code !== 200 && $code !== 204) {
                    throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
                }
            } catch (Exception $e) {
                $response['err'] = 'Ошибка: ' . $e->getMessage() . PHP_EOL . ' Код ошибки: ' . $e->getCode();
            }

            // Данные получаем в формате JSON, поэтому, для получения читаемых данных,
            // нам придётся перевести ответ в формат, понятный PHP
            $response = json_decode($out, true);

        } catch (Exception $e) {
			$response['err'] = 'Ошибка: ' . $e->getMessage() . PHP_EOL . ' Код ошибки: ' . $e->getCode();
        }

        return $response;
    }
}
