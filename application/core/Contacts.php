<?php

class Contacts extends Entity {

    public function __construct($number, $param)
    {
        $this->nameEntity = 'contacts';
        $this->number = $number;
        $this->param = $param;
        $this->link = API_CONTACTS;
        parent::__construct($this->nameEntity, $number, $param, $this->link);
    }
}
