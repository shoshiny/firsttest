<?php

class Tasks extends Entity {

    public function __construct($number, $param)
    {
        $this->nameEntity = 'tasks';
        $this->number = Control::checkValue($number);
        $this->param = $param;
        $this->link = API_TASKS;
    }

/* @TODO remove
    public function setObjTask($arr, $action) {
    }
*/
}
