<?php

class Auth {

    public function authorisation()
    {
        $user = [
            'USER_LOGIN' => LOGIN,
            'USER_HASH' => HASH
        ];
        $response = Request::requestApi(API_AUTH, $user);

        if (!empty($response['response'])) {
            $response = $response['response'];

            if (isset($response['auth'])) {
                $message = 'Авторизация прошла успешно ' .
                            date('H:i:s', time() + 3600) .
                            ' --> ' .
                            microtime() .
                            '<br>';
            } else {
                $message = 'Авторизация не удалась';
            }
        } elseif (!empty($response['err'])) {
            $message = $response;
        }

        return $message;
    }

    public function tryAuth()
    {
		$result = [];
		$result['mess'] = Auth::authorisation();

        if ($result['mess'] == 'Авторизация не удалась') {
			$result['auth'] = '';
		} else {
			$result['auth'] = true;
        }

		return $result;
    }

}
