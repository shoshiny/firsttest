<?php

class Model {
    
    public function getData()
    {
    }

    public function validateFields($post, $postFields = null, $negation = false)
    {
        $postKeys = array_keys($post);
        
        if (!is_array($postFields)) {
            $postFields = str_replace(' ', '', $postFields);
            $postFields = explode(',', $postFields);
        }

        $result = false;

        foreach ($postFields as $postKey) {
            // array_key_exists($postKey, $allFields);
            if (!$negation ? !array_key_exists(trim($postKey), array_diff($postKeys, $postFields)) : 
                array_key_exists(trim($postKey), $postKeys)) {
                // this validate post fields by not empty in post value
                if (!$negation && empty($post[$postKey])) {
                    $result = false;
                    break;
                }
                $result = true;
            } else {
                $result = false;
                break;
            }
        }

        return $result;
    }

}
