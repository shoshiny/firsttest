<?php

class Notes extends Entity {

    public $callParams;

    public function __construct($number, $param, $typeEntity)
    {
        $this->nameEntity = 'notes';
        $this->number = Control::checkValue($number);
        $this->param = $param;
        $this->link = API_NOTES;
        $this->toEntityType = $typeEntity;
    }

    /**create obj note
     * @param $entityId
     * @param $noteTypeId
     * @param $addText
     * @return array
     */
    public function setNoteObj($entityId, $noteTypeId, $addText)
    {
        $this->objEntity = [
            'add' => [
                [
                    'element_id' => $entityId,
                    'element_type' => $this->toEntityType,
                    'note_type' => $noteTypeId,
                    'text' => $addText
                ]
            ]
        ];
        $this->setCallParams($addText);

        return $this->objEntity;
    }

    /**set params call
     * @param $phoneNumber
     */
    public function setCallParams($phoneNumber)
    {
        $uniq = str_replace('=', '',
                        str_replace('+', '',
                            base64_encode(
                                md5(
                                    mt_rand(1000000, 99999999999)
                                )
                            )
                        )
                    );
        $dom = substr($uniq, mt_rand(0, 5), mt_rand(6, 12));
        $this->callParams = [
            'params' => [
                'UNIQ' => strtoupper(substr($uniq, 0, 5)).
                    mt_rand(1000, 9999),
                'DURATION' => mt_rand(15, 600),
                'SRC' =>
                    'http://'.
                    strtolower($dom).
                    '.com/example/'.
                    $phoneNumber.
                    '_'.
                    time().
                    '.mp3',
                'LINK' =>
                    'http://'.
                    strtolower($dom).
                    '.com/example/'.
                    $phoneNumber.
                    '_'.
                    time().
                    '.mp3',
                'PHONE' => $phoneNumber
            ]
        ];

        $this->callParams = [
            'params' => [
                'UNIQ' => strtoupper(substr($uniq, 0, 5)).
                    mt_rand(1000, 9999),
                'DURATION' => 9,
                'SRC' =>
                    'https://cf-hls-media.sndcdn.com/media/318901/478561/8IsFZS7TMDjy.128.mp3?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiKjovL2NmLWhscy1tZWRpYS5zbmRjZG4uY29tL21lZGlhLyovKi84SXNGWlM3VE1EankuMTI4Lm1wMyIsIkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTU2MzM2ODU1NX19fV19&Signature=Zd8mtD4g2Pi6N26KvnDXzleBU6BOhrIBwypHnRWck7h0iL-1kvJ7SkEd8FrS8IUAUmdhWmis1kVwCCMYaH65ceMqCWS4D9SPaP7fFInqXdnA7xX9lc2YcUx2G3SefOqOcciuMl3Wqor5ipIjUn9JqeQXx~lYQIRkgsNnnf27rTHeUqFY6XcpW2FHveFEgjx3RBzAJcGswVbrDZ94tEhWAWfpCoJR1Z5zr4tYL6Q1q4pMP8DqsVWPukMZvtQ2m~JX2vrTUfRPV1s0pie2q-MQzv5H716-8dhXDiFk2gtdKZRYxOcq7Tp4F~n9FYuEmUPSLzaTRbguh7GQjBrOSmS3Tg__&Key-Pair-Id=APKAI6TU7MMXM5DG6EPQ',
                'LINK' =>
                    'https://cf-hls-media.sndcdn.com/media/318901/478561/8IsFZS7TMDjy.128.mp3?Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiKjovL2NmLWhscy1tZWRpYS5zbmRjZG4uY29tL21lZGlhLyovKi84SXNGWlM3VE1EankuMTI4Lm1wMyIsIkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTU2MzM2ODU1NX19fV19&Signature=Zd8mtD4g2Pi6N26KvnDXzleBU6BOhrIBwypHnRWck7h0iL-1kvJ7SkEd8FrS8IUAUmdhWmis1kVwCCMYaH65ceMqCWS4D9SPaP7fFInqXdnA7xX9lc2YcUx2G3SefOqOcciuMl3Wqor5ipIjUn9JqeQXx~lYQIRkgsNnnf27rTHeUqFY6XcpW2FHveFEgjx3RBzAJcGswVbrDZ94tEhWAWfpCoJR1Z5zr4tYL6Q1q4pMP8DqsVWPukMZvtQ2m~JX2vrTUfRPV1s0pie2q-MQzv5H716-8dhXDiFk2gtdKZRYxOcq7Tp4F~n9FYuEmUPSLzaTRbguh7GQjBrOSmS3Tg__&Key-Pair-Id=APKAI6TU7MMXM5DG6EPQ',
                'PHONE' => $phoneNumber
            ]
        ];
/*
        // default example :
        $this->callParams = [
            'params' => [
                'UNIQ' => 'BCEFA2341',
                'DURATION' => 33,
                'SRC' => 'http://example.com/calls/1.mp3',
                'LINK' => 'http://example.com/calls/1.mp3',
                'PHONE' => $phoneNumber
            ]
        ];
*/
    }

    /**get callParams
     * @return array
     */
    public function getCallParams()
    {
        return $this->callParams;
    }

}
