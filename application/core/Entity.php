<?php

class Entity {

    private $objEntity;
    private $nameEntity;
    private $number;
    private $param;
    private $link;
    private $resultPushEntity; //result push entity
    private $idEntity; //entity id
    private $toEntityType; //entity id
    private $arrayData;

    public function __construct($nameEntity, $number, $param, $link)
    {
        $this->nameEntity = $nameEntity;
        $this->number = Control::checkValue($number);
        $this->param = $param;
        $this->link = $link;
        $this->objEntity = Entity::genRandomNames($this->nameEntity, $this->number);
    }

    /**set object entity
     * @param $arrKey
     * @param $action
     */
    public function setObjEntity($arrKey, $action)
    {
//        $arrKey - array of keys end request
        $result = [];

        foreach ($arrKey as $key => $val) {
            $result[$key] = $val;
        }

        $preResult[$action] = [$result];
        $this->arrayData = $preResult;
    }

    /**get object entity
     * @return mixed
     */
    public function getObjEntity()
    {
        return $this->arrayData;
    }

    /**add id to entity
     * @param $arrEntity
     * @param $arrId
     * @param string $addId
     * @return mixed
     */
    public function addIdEntity($arrEntity, $arrId, $addId = null)
    {
        if (empty($addId)) {
            $addId = 'company_id';
        }

        $result = $arrEntity;
        $i = 0;

        foreach ($result as $val) {
            if (!empty($result[$i]['name'])) {
                foreach ($arrId as $val2) {
                    if ($addId === 'contacts_id') {
                        $result['add'][$i][$addId] = [$val2];
                    } else {
                        $result['add'][$i][$addId] = $val2;
                    }

                    $i++;
                }
            }
        }

        return $result;
    }

    /**new method for create Entity
     * @param $setentity
     */
    public function setEntity($setentity)
    {
        $this->objEntity = $setentity;
    }

    /**
     * @return array
     */
    public function getEntity()
    {
        return $this->objEntity;
    }

    /**some name generation array
     * @param $nameEntity
     * @param $number
     * @param null $upBound
     * @return array
     */
    public function genRandomNames($nameEntity, $number, $upBound = null)
    {
        $randomNames = [];

        if (empty($upBound)) {
            $upBound = 10000;
        }

        $i = 1;

        while ($i <= $number) {
            if (strstr($nameEntity, 'fields')) {
                $randomNames[$nameEntity][] = [
                    "name" => 'Name '.
                        $nameEntity.
                        ' '.
                        $i.
                        ' '.
                        mt_rand(0, $upBound).
                        ' '.
                        mt_rand(0, $upBound)
                ];
            } else {
				//new API needs data in request else will not create customers
				if ($nameEntity == 'customers') {
                $randomNames[$nameEntity][] = [
                    "name" => 'Some name '.
                        $nameEntity.
                        ' '.
                        $i.
                        ' '.
                        mt_rand(0, $upBound).
                        ' '.
                        mt_rand(0, $upBound),
					"next_date" => time()
                ];
				} else {
                $randomNames[$nameEntity][] = [
                    "name" => 'Some name '.
                        $nameEntity.
                        ' '.
                        $i.
                        ' '.
                        mt_rand(0, $upBound).
                        ' '.
                        mt_rand(0, $upBound)
					];
				}
            }
            $i++;
        }

        $resultRandomNames[$this->param] = $randomNames[$nameEntity];

        return $resultRandomNames;
    }

    /**puch entiteis to api, set limit here
     * @param $arrRequest
     * @return array
     */
    public function pushEntity($arrRequest)
    {
        $limit = 100; // 100 - optimal size of array
        $step = Control::getStepFromSizeArr($arrRequest, $limit);
        $preResult = Control::devideArrays($arrRequest, $limit);
        $response = Control::queryRestriction($preResult, $step, $this->link);

        if (isset($response['_embedded']['items'])) {
            $response = $response['_embedded']['items']; // mb del
        }

        return $response;
    }

    /**set result after push
     * @param $arrRequest
     */
    public function setResultPushEntity($arrRequest)
    {
        $this->resultPushEntity = $this->pushEntity($arrRequest);
    }

    /**get result after push
     * @return array
     */
    public function getResultPushEntity()
    {
        return $this->resultPushEntity;
    }

    /**set id new create Entities
     * @param $arrEntity
     * @return array
     */
    public function setArrayIdNewEntity($arrEntity)
    {
        $result = [];
        $i = 0;

        if (!empty($arrEntity) && is_array($arrEntity)) {
            foreach ($arrEntity as $val) {
                if (isset($val[$i]) && isset($val[$i]['id']) && is_array($val[$i])) {
                    $result[] = $val[$i]['id'];
                } elseif (isset($val['id'])) {
                    $result[] = $val['id'];
                }

                $i++;
            }
        }

        return $result;
    }

    /**add id to entity
     * @param $arr
     * @param $arrId
     * @param null $keyName
     * @return array
     */
    public function addIdToEntity($arr, $arrId, $keyName = null)
    {
        if (empty($keyName)) {
            $keyName = 'company_id';
        }

        $i = 0;

        foreach ($arr as $val) {
            if (!empty($arr[$this->param][$i]['name'])) {
                foreach ($arrId as $val2) {
                    if ($keyName === 'contacts_id') {
                        $arr[$this->param][$i][$keyName] = [$val2];
                    } else {
                        $arr[$this->param][$i][$keyName] = $val2;
                    }
                    $i++;
                }
            }
        }

        return $arr;
    }

    /**set id entities
     * @param $arrId
     */
    public function setEntityId($arrId)
    {
        $this->idEntity = $arrId;
    }

    /**get id entities
     * @return mixed
     */
    public function getIdEntity()
    {
        return $this->idEntity;
    }

    /**get one entity by ID
     * @param $link
     * @return array
     */
    public function getOneEntitiyId($link)
    {
        $preResult = [];

        if (!empty(sizeof($result = Request::requestApi($link)))) {
            $preResult = $result['_embedded']['items'][0];
        }

        $result = [$preResult];

        return $result;
    }

    /**get all entities by id
     * @param $link
     * @return array
     */
    public function getAllEntitiesId($link)
    {
        $i = 0;
        $preResult = [];

        while (!empty($result = Request::requestApi($link, '', $i * 500)) && is_array($result) && sizeof($result) > 0) {
            $preResult = array_merge($preResult, $result['_embedded']['items']);

            if ($i % 5 === 0) {
				sleep(1);
			}

            $i++;
            $result = null;
            // uncomment line down to save log add entites
			// file_put_contents("./tracer.log", "\$i := $i, \$i*500 := " . ($i*500) . "\n", 8);
        }

        // uncomment line down to save log add entites
        // file_put_contents("./tracer.log", "\n--" . date("H:i:s", time() + 3600) . "--\n", 8);
        $result = $preResult;

        return $result;
    }

    /**get data entities by id
     * @param $link
     * @param $id
     * @return array
     */
    public function getDataEntitiesById($link, $id)
    {
        $preResult = [];

        if (!empty(sizeof($result = Request::requestApi($link, '', '', $id)))) {
            $preResult = array_merge($preResult, $result['_embedded']['items']);
        }

        $result = $preResult;

        return $result;
    }

    /**type to api_link
     * @param $key
     * @return array
     */
    public function entTypeToApi($key)
    {
        switch ($key) {
            case 2:
                $result = ['leads', API_LEADS];
                break;
            case 3:
                $result = ['companies', API_COMPANIES];
                break;
            case 4:
                $result = ['fields', API_FIELDS];
                break;
            case 5:
                $result = ['notes', API_NOTES];
                break;
            case 6:
                $result = ['calls', API_CALLS];
                break;
            case 62:
                $result = ['calls', API_CALLS2];
                break;
            case 7:
                $result = ['tasks', API_TASKS];
                break;
            case 12:
                $result = ['customers', API_CUSTOMERS];
                break;

            default:
                $result = ['contacts', API_CONTACTS];
                break;
        }

        return $result;
    }

    /**test set entity in server
     * @param $id
     * @param $entityType
     * @return array
     */
    public function tryEntity($id, $entityType)
    {
        $ent = $this->entTypeToApi($entityType);
        $addlink = $ent[1] . '?id=' . $id;

        if (!empty(sizeof($result = Request::requestApi($addlink)))) {
            $result = [
                'reply' => true,
                'r_obj' => $result
            ];
        } else {
            $result = [
                'reply' => false,
                'r_obj' => null
            ];
        }

        return $result;
    }

    /**fast add one custom fields
     * @param $entityType
     * @param $request
     * @return array
     */
    public function entityOneQuickRequest($entityType, $request)
    {
        $ent = $this->entTypeToApi($entityType);
        $addlink = $ent[1];
        $result = Request::requestApi($addlink, $request);

        return $result;
    }

}
