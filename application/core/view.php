<?php

class View {

    public $pageTitle = 'Index';

    public function generate($contentView, $templateView, $data = null)
    {
        if (is_array($data)) {
            extract($data);
        }

        require_once 'application/views/' . $templateView;
    }

    /**set page title
     * @param $newtitle
     */
    public function setPageTitle($newTitle)
    {
        $this->pageTitle = $newTitle;
    }

    /**get page title
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }
}
