<?php

class CustomFields extends Entity {

    public $customFieldsValuesId;
    public $randomCustomFieldsValueArray;

    public function __construct($number, $param)
    {
        $this->nameEntity = 'fields';
        $this->number = Control::checkValue($number);
        $this->param = $param;
        $this->link = API_FIELDS;
        parent::__construct($this->nameEntity, $number, $param, $this->link);
    }

    /**2. create Arr custom fields
     * @param $entity
     * @param $number
     * @param $typeField
     * @param $typeElement
     * @param $origin
     * @param null $inputValue
     * @return array
     */
    public function generateCustomField($entity, $number, $typeField, $typeElement, $origin, $inputValue = null)
    {
        $i = 0;

        foreach ($entity['add'] as $key => $val) {
            $entity['add'][$key]['type'] = $typeField;
            $entity['add'][$key]['element_type'] = $typeElement;
            $entity['add'][$key]['origin'] = $origin;
            $entity['add'][$key]['is_editable'] = 1;

            if ((int) $typeField === 5) {
                unset($entity['add'][$key]['type']);
                $entity['add'][$key]['field_type'] = $typeField;

                while ($i < $number) {
                    $entity['add'][$key]['enums'][] = 'Value ' . ($i + 1) . ' ' . mt_rand(0, 1000);
                    $i++;
                }
            } elseif ((int) $typeField === 1) {
                $entity['add'][$key]['values']['value'][] = $inputValue;
            }
        }

        return $entity;
    }

    /**3. add custom fields to contact by arr_ID
     * @param $entity
     * @return array
     */
    public function addFieldsToEntity($entity)
    {
        return Request::requestApi($this->link, $entity);
    }

    /**set custom fields id
     * @param $arr
     */
    public function setCustomFieldsId($arr)
    {
        $this->idEntity = $arr;
    }

    /**get custom fields id
     * @return array
     */
    public function getCustomFieldsId()
    {
        return $this->idEntity;
    }

    /**get all custom fields
     * @return array
     */
    public function getAllCustomFields()
    {
        return Request::requestApi(API_ACCOUNT);
    }

    /**set custom fields id
     *
     */
    public function setCustomFieldsValueId()
    {
        $arrayRequest = $this->getAllCustomFields();
        $customFieldsId = $this->idEntity;
        $arrayValues = [];

        if (is_array($customFieldsId) && !empty($customFieldsId[0])) {
            foreach ($customFieldsId as $key => $val) {
                $arrayValues[] = $arrayRequest['_embedded']['custom_fields']['contacts'][$val]['enums'];
            }
        }

        $this->customFieldsValuesId = $arrayValues;
    }

    /**get custom values
     * @return mixed
     */
    public function getCustomFieldsValueId()
    {
        return $this->customFieldsValuesId;
    }

    /**4. set random values custom fields
     *
     */
    public function setRandomCustomFieldsValue()
    {
        $tempArray = [];
        $arrayValues = $this->getCustomFieldsValueId();

        if (is_array($arrayValues) && !empty($arrayValues[0])) {
            foreach ($arrayValues as $key) {
                foreach ($key as $k => $val) {
                    $tempArray[] = $k;
                }
            }
        }

        $randomLimit = sizeof($tempArray) - 1;
        $limit = mt_rand(1, $randomLimit);

        for ($i = 0;$i <= $limit; $i++) {
            $rnd = mt_rand(0, $randomLimit);
            $resultArray[] = $tempArray[$rnd];
        }

        $this->randomCustomFieldsValueArray = $resultArray;
    }

    /**get one random arr
     * @return array
     */
    public function getRandomCustomFieldsValue()
    {
        return $this->randomCustomFieldsValueArray;
    }

    /**set N random arr
     * @param $number
     */
    public function setGenerationRandomArrays($number)
    {
        $number = (int) $number;
        $preResult = [];

        for ($i = 0; $i < $number; $i++) {
            $this->setRandomCustomFieldsValue();
            $preResult[] = $this->getRandomCustomFieldsValue();
        }

        $this->randomCustomFieldsValueArray = $preResult;
    }

    /**set to rnd_val_arr to arr
     * @param $arr
     */
    public function setRandomArraysValues($arr)
    {
        $this->randomCustomFieldsValueArray = $arr;
    }

    /**get to rnd_val_arr to arr
     * @return mixed
     */
    public function getRandomArraysValues()
    {
        return $this->randomCustomFieldsValueArray;
    }

    /**create result update array
     * @param $customFieldsArrayId
     * @param string $typeField
     * @return array
     */
    public function createResultCustomFieldsUpdateArray($customFieldsArrayId, $typeField = '')
    {
        $resultArray = $this->randomCustomFieldsValueArray;
        // $customFieldsId = $this->getIdEntity();

        if (sizeof($customFieldsArrayId) === sizeof($resultArray)) {
            $i = 0;

            foreach ($customFieldsArrayId as $key => $val) {
                if ((int) $typeField === 5) {
                    $result['update'][$i] = [
                        'id' => $customFieldsArrayId[$i],
                        'updated_at' => time() + 10800,
                        'custom_fields' => [
                            [
                            'id' => $this->idEntity[0],
                            'values' => $resultArray[$i]
                            ]
                        ]
                    ];
                } elseif (!empty($resultArray['add']) && !empty($resultArray['add'][$i]['origin'])) {
                    $result['update'][$i] = [
                        'id' => $resultArray['add'][$i]['origin'],
                        'updated_at' => time() + 10800,
                        'custom_fields' => [
                            [
                                'id' => $customFieldsArrayId[$i],
                                'values' => [
                                    [
                                        'value' => $resultArray['add'][$i]['values']['value'][0]
                                    ]
                                ]
                            ]
                        ]
                    ];
                } else {
                    $result['update'][$i] = [
                        'id' => $customFieldsArrayId[$i],
                        'updated_at' => time()+10800,
                        'custom_fields' => [
                            [
                                'id' => $this->idEntity[0],
                                'values' => [
                                    [
                                        'value' => $resultArray[$i]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }

                $i++;
            }
        }

        return $result;
    }

    /**test set fields in accounts
     * @param $id
     * @param $entityType
     * @return array $result
     */
    public function tryCustomFieldsAccount($id, $entityType)
    {
        $entity = Entity::entTypeToApi($entityType);

        if (!empty(sizeof($result = CustomFields::getAllCustomFields()))) {
            $result = [
                'reply' => true,
                'result_fields' => $result,
                'result_entity' => $result['_embedded']['custom_fields'][$entity[0]]
            ];
        } else {
            $result = [
                'reply' => false,
                'result_fields' => null
            ];
        }

        return $result;
    }

    /**get id text fields
     * @param $arr
     * @return array $result
     */
    public function getIdTextFields($arr)
    {
        $result = [];

        foreach ($arr as $val) {
            if (!empty($val['field_type']) && ((int) $val['field_type'] === 1)) {
                $result[] = $val['id'];
                break;
            }
        }

        return $result;
    }

}
