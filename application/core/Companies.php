<?php

class Companies extends Entity {

    public function __construct($number, $param)
    {
        $this->nameEntity = 'companies';
        $this->number = $number;
        $this->param = $param;
        $this->link = API_COMPANIES;
        parent::__construct($this->nameEntity, $number, $param, $this->link);
    }
}
