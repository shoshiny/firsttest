<?php

class Leads extends Entity {

    public function __construct($number, $param)
    {
        $this->nameEntity = 'leads';
        $this->number = $number;
        $this->param = $param;
        $this->link = API_LEADS;
        parent::__construct($this->nameEntity, $number, $param, $this->link);
    }
}
