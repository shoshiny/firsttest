<?php

class Autoloader {

    public static function register()
    {
        spl_autoload_register(function ($class)
        {
            $dirs = [
                'core' => '',
                'models' => 'class.',
                ];

            foreach ($dirs as $folder => $fileFolder) {
                $file = 'application' .
                        SEPARATOR .
                        $folder .
                        SEPARATOR .
                        $fileFolder .
                        str_replace(SEPARATOR, DIRECTORY_SEPARATOR, $class) .
                        '.php';

                if (file_exists($file)) {
                    require_once($file);
                    break;
                } elseif (file_exists(strtolower($file))) {
                    require_once(strtolower($file));
                    break;
                }
            }
        });
    }
}

Autoloader::register();
