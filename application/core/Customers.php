<?php

class Customers extends Entity {

    public function __construct($number, $param)
    {
        $this->nameEntity = 'customers';
        $this->number = $number;
        $this->param = $param;
        $this->link = API_CUSTOMERS;
        parent::__construct($this->nameEntity, $number, $param, $this->link);
    }
}
