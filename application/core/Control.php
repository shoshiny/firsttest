<?php

class Control {

    /**validation number
     * @param $numb
     * @return int
     */
    public function checkValue($numb)
    {
        $resultNumber = 0;

        if (is_numeric($numb)) {
            $resultNumber = (int) $numb;

            if ($resultNumber < 0) {
                $resultNumber = 1;
            } elseif ($resultNumber > 10000) {
                $resultNumber = 10000;
            }
        }

        return $resultNumber;
    }

    /**Balance request to API, create restrict by step
     * @param $request
     * @param $step
     * @param null $link
     * @param null $action
     * @return array
     */
    public function queryRestriction($request, $step, $link = null, $action = null)
    {
        $response = [];
        $auth = Auth::authorisation();

        if (!is_array($auth)) {
            $response = ['auth' => $auth];
        } else {
            $response = $auth;
        }

        if (empty($action)) {
            $action = 'add';
        }

        $preResult = [];
        $flag = false;
        $stepFlags = $step;

        if ((int) $stepFlags === 1) {
            $flag = true;
        }

        $i = 0;
        $localCount = 0;

        while ($step > 0) {
            $step--;
            $send = [$action => $request[$i]];
            $preResult = Request::requestApi($link, $send);

            if (!empty($preResult['_embedded']['items'])) {
                $response = array_merge($response, $preResult['_embedded']['items']);
            } else {
                break;
            }

            if ($localCount % 5 === 0) {
                sleep(1);
            } else {
                time_nanosleep(0, 145000000); // 145000000
            }

            $i++;
            $localCount++;
        }

        return $response;
    }

    /** devide array input to get step for next functions
     * @param $request
     * @param $limit
     * @param null $action
     * @return int
     */
    public function getStepFromSizeArr($request, $limit, $action = null)
    {
        if (empty($action)) {
            $action = 'add';
        }

        $count = sizeof($request[$action]);

        if ($count < $limit) {
            $step = 1;
        } elseif ($count % $limit > 0) {
            $step = ($count - $count % $limit) / $limit + 1;
        } else {
            $step = ($count - $count % $limit) / $limit;
        }

        return $step;
    }

    /**function to devide array by set limit
     * @param $request
     * @param $limit
     * @param null $action
     * @return array
     */
    public function devideArrays($request, $limit, $action = null)
    {
        if (empty($action)) {
            $action = 'add';
        }

        $result = array_chunk($request[$action], $limit, true);

        return $result;
    }

}
