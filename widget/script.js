define(['jquery'], function($){
    var CustomWidget = function () {
        var self = this;
        system = self.system;

        this.get_llist_info = function () {
            if (self.system().area == 'llist') {
                var leads = $("input:checkbox:checked[id^=lead_]");
                var t = [];
                for (var i = 0; i < leads.length; i++) {
                    t[i] = $(leads[i]).val();
                }
                return t;
            }
        }

        this.my_send_function = function(param) {
            var surl = "http://127.0.0.1";
            $.ajax({
                url: surl + '/g1widget',
                type: "POST",
                data: "ids=" + param,
                timeout: 30000,
                beforeSend:function() {},
                success:function(data) {console.log(data)},
                error:function(xhr) {console.log('some error');console.log(xhr);}
            });
            console.log(param);
        }

        this.my_function = function () {
            if (self.system().area == 'llist') {
                console.log('it\'s work');
            }
        } 

        this.callbacks = {
            render: function() {
                console.log('render');
                return true;
            },
            init: function() {
                console.log('init');
                return true;
            },
            bind_actions: function() {
                console.log('bind_actions');
                return true;
            },
            settings: function() {
                return true;
            },
            onSave: function() {
                return true;
            },
            destroy: function() {

            },
            contacts: {
                //select contacts in list and clicked on widget name
                selected: function() {
                    console.log('contacts');
                }
            },
            leads: {
                //select leads in list and clicked on widget name
                selected: function() {
                    console.log('leads');
                    self.my_send_function(self.get_llist_info());
                    self.my_function();
                }
            },
            tasks: {
                //select taks in list and clicked on widget name
                selected: function() {
                    console.log('tasks');
                }
            }
        };
        return this;
    };

    return CustomWidget;
});